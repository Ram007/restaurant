import React from 'react';
import {Link} from 'react-router-dom'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  } from 'reactstrap';
import injectsheet from 'react-jss'
import Headroom from 'react-headroom'

  const styles={
    navbarTop:{
      width: '100%',
      position: 'relative',
      '& .headroom':{
        top: '0px',
        left: '0px',
        right: '0px',
        zIndex: '1',
      },
      '& .headroom--unfixed': {
        position: 'relative',
        transform: 'translateX(0)',
      },
      '& .navbar':{
        padding: '1rem 1rem',
      },
      '& .bg-light':{
        backgroundColor: '#f7f7f7!important',
      },
      '& a':{
        color: '#111111!important',
        fontFamily: 'montserrat',
        fontWeight: '600',
        transition: 'all 0.4s ease 0s',
        '&:hover':{
          color: '#fcc02e!important',
          borderRadius: '5px',
          backgroundColor: '#040303e6',
          lineHeight: 'inherit',
        }
      },
      '& .navbar-brand':{
          marginLeft: '6rem',
          padding: '0',
          maxWidth: '150px',
          '@media screen and (max-width: 768px)':{
            marginLeft: '0',
           },
           '&:hover':{
             backgroundColor: 'inherit',

           }
      },
      '& .navbar-collapse':{
          flexGrow: '0.75',
          fontSize: '1rem',
      },
    }
  }

class NavbarMenu extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    const {classes} = this.props;
    return (
      <div className={classes.navbarTop}>
        <Headroom>
          <Navbar color="light" light expand="md">
            <NavbarBrand href="/">Restourant</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <li className="nav-item">
                  <Link className="nav-link"  to="/">HOME</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link"  to="/about">ABOUT US</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link"  to="/contact">CONTACT</Link>
                </li>
              </Nav>
            </Collapse>
          </Navbar>
        </Headroom>
      </div>
    );
  }
}

export default injectsheet(styles) (NavbarMenu);