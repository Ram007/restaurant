import React, {Component} from 'react'
import injectsheet from 'react-jss'

const styles={
    footer:{
        width: '100%',
        position: 'relative',
    }
}

class Footer extends Component{
    render(){
        const {classes} = this.props;
        return(
            <div className={classes.footer}>
                <h1>Footer</h1>
            </div>
        )
    }
}

export default injectsheet(styles) (Footer);