import React, {Component} from 'react';
import Slider from 'react-slick'
import injectsheet from 'react-jss'

const styles = {
    slider:{
        width: '100%',
        position: 'relative',
    }
}

class SliderMenu extends Component {
    render(){
        const {classes} = this.props;
        const settings = {
            infinite: true,
            arrows: false,
            autoplay: true,
            dots: false,
            speed: 500,
            slidesToShow: 1,
            fade: true,
          };
        return(
            <div className={classes.slider}>
                <Slider {...settings}>
                    <div > <img src="/images/1.jpg" alt="" width="100%"/></div>
                    <div > <img src="/images/2.jpg" alt="" width="100%"/></div>
                    <div > <img src="/images/3.jpg" alt="" width="100%"/></div>
                </Slider>
            </div>
            
        );
    }
}
export default injectsheet(styles)(SliderMenu);