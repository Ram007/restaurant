import React, {Component} from 'react'
import injectsheet from 'react-jss'

const styles={
    banner:{
        width: '100%',
        position: 'relative',
        '& img':{
            height: '90vh',
        }
    }
}

class Banner extends Component{
    render(){
        const {classes} = this.props;
        return(
            <div className={classes.banner}>
                <img src="/images/1.jpg" alt="" width="100%"/>
            </div>
        )
    }
}

export default injectsheet(styles) (Banner);