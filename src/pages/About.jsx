import React, {Component} from 'react'
import injectsheet from 'react-jss'

const styles={
    about:{
        width: '100%',
        position: 'relative',
    }
}

class About extends Component{
    render(){
        const {classes} = this.props;
        return(
            <div className={classes.about}>
                <h1>About Us</h1>
            </div>
        )
    }
}

export default injectsheet(styles) (About);