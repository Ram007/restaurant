import React, { Component} from 'react'
import injectsheet from 'react-jss'
import Banner from '../component/SliderMenu'
import BannerMenu from '../component/Banner'

const styles={
    home:{
        textAlign: 'center',
        position: 'relative',
    }
}

class Home extends Component{
    render(){
        const {classes} = this.props;
        return(
            <div className={classes.home}>
               {/*<Banner/> */} 
               <BannerMenu/>
            </div>
        )
    }

}

export default injectsheet(styles) (Home);