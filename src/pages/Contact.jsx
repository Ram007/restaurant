import React, {Component} from 'react'
import injectsheet from 'react-jss'

const styles={
    contact:{
        width: '100%',
        position: 'relative',
    }
}

class Contact extends Component{
    render(){
        const {classes} = this.props;
        return(
            <div className={classes.contact}>
                <h1>Contact</h1>
            </div>
        )
    }
}

export default injectsheet(styles) (Contact);