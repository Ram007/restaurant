import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import NavbarMenu from './component/NavbarMenu'
import Home from './pages/Home'
import About from './pages/About'
import Contact from './pages/Contact'
import Footer from './component/Footer' 

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
            <NavbarMenu/>
            <div>
                <Route exact path="/" component={Home} />
                <Route exact path="/about" component={About} />
                <Route exact path="/contact" component={Contact} />
            </div>
            <Footer/>
        </Router>
      </div>
    );
  }
}

export default App;
